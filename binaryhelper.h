#pragma once

#include <vector>
#include <bitset>
#include <string>

#define NUMBER_OF_VERTEXES ((1<<25) - 1) // 2^24

class Function {
private:
    std::string formula;
    std::string typeOfFunction;
public:
    Function(std::string &formula, std::string &typeOfFunction);

    std::string &getFormula();

    std::string &getTypeOfFunction();
};

bool calculateValue(unsigned int function, unsigned int vector);

unsigned int getCurrentStatePointer(std::vector<std::vector<bool> > &state);

int getFirstNonZeroBit(std::bitset<NUMBER_OF_VERTEXES> &currentBitset, int size);

void setCurrentStatePointer(unsigned int number,
                            std::vector<std::vector<bool> > &state);

std::string stringBinaryForBoolean(unsigned int function, unsigned int numberOfVariables);

unsigned int weightForFunction(unsigned int function, unsigned int numberOfVariables);

std::string stringBinaryForIntegerWithLenght(unsigned int value, unsigned int length);

std::vector<bool> ghegalkin(unsigned int function, unsigned int numberOfVariables);

std::string getAnfDescription(std::vector<bool> &polynomCoef, int numberOfVariables);

std::vector<bool> toReverseOrder(std::vector<bool> source);

std::vector<bool> integerToBooleanVector(unsigned int value, unsigned int size);

Function getFormulaFromAnf(std::vector<bool> &anf, std::vector<std::string> &literals);

std::string getTypeOfFunctionFromMask(int mask, int countOfCoefs);