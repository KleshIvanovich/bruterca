#ifndef BRUTE_RCA_CRYPTO_ANALYSER_H
#define BRUTE_RCA_CRYPTO_ANALYSER_H
#include "automate.h"


class RcaCryptoAnalyzer {
public:
    explicit RcaCryptoAnalyzer(Automate & automate);
    float averageCycleLength() const;
    int shiftedCyclesCount();
    int shiftedCyclesLength();
    float averageFixedPoints() const;
    float differentialChar();
    float lightDifferentialChar();
    bool isCycleShifted(std::vector<unsigned int> cycle);
private:
    Automate automate;
    float totalCyclesCount;
    float totalFixedPointsCount;
    int totalShiftedCyclesCount;
    int totalShiftedCyclesLength;
};


#endif //BRUTE_RCA_CRYPTO_ANALYSER_H
