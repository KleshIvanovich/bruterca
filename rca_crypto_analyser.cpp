#include "rca_crypto_analyser.h"
#include <omp.h>
#include <iostream>

#define MAX_LENGHT_FOR_LIGHT_DIFF_ANALYSIS 3

bool equalWithShift(unsigned int first, unsigned int second);

RcaCryptoAnalyzer::RcaCryptoAnalyzer(Automate &automate) : automate(automate) {
    totalCyclesCount = 0;
    totalFixedPointsCount = 0;

    totalShiftedCyclesCount = 0;
    totalShiftedCyclesLength = 0;

    automate.setStatePointer(0);
    unsigned int cycleStartPointer = getCurrentStatePointer(automate.getCurrentState());
    while (true) {
        int cycleLength = 0;
        std::vector<unsigned int> currentCyclePoints = std::vector<unsigned int>();
        while (true) {
            automate.applyFunctionStep();
            unsigned int currentStatePointer = getCurrentStatePointer(automate.getCurrentState());
            automate.vertexes->set(currentStatePointer);

            if (currentStatePointer == cycleStartPointer) {
                if (cycleLength == 0) {
                    totalFixedPointsCount++;
                    break;
                }
                if (isCycleShifted(currentCyclePoints)) {
                    totalShiftedCyclesCount++;
                    totalShiftedCyclesLength += currentCyclePoints.size();
                }
                break;
            }
            cycleLength++;
            currentCyclePoints.push_back(currentStatePointer);
        }

        totalCyclesCount++;

        int newCyclePointer = automate.getNewCyclePointer();

        if (newCyclePointer != -1) {
            cycleStartPointer = newCyclePointer;
            automate.setStatePointer(cycleStartPointer);
            continue;
        } else {
            break;
        }
    }
}

float RcaCryptoAnalyzer::averageCycleLength() const {
    return ((float) (1 << automate.dimensionX)) / totalCyclesCount;
}

int RcaCryptoAnalyzer::shiftedCyclesCount() {
    return totalShiftedCyclesCount;
}

int RcaCryptoAnalyzer::shiftedCyclesLength() {
    return totalShiftedCyclesLength;
}

float RcaCryptoAnalyzer::differentialChar() {
    unsigned int pointersSize = (1 << automate.dimensionX);
    float maxProbability = 0;
#pragma omp parallel for
    for (unsigned int a = 1; a < pointersSize; a++) {
        for (unsigned int b = 0; b < pointersSize; b++) {
            float collisionsCounter = 0;
            for (unsigned int x = 0; x < pointersSize; x++) {
                automate.setStatePointer(x);
                automate.applyFunctionStep();
                unsigned int F_x = getCurrentStatePointer(automate.getCurrentState());

                automate.setStatePointer(x ^ a);
                automate.applyFunctionStep();
                unsigned int F_x_a = getCurrentStatePointer(automate.getCurrentState());

                if ((F_x ^ F_x_a) == b) {
                    collisionsCounter++;
                }
            }
#pragma omp critical
            if (collisionsCounter / pointersSize > maxProbability) {
                maxProbability = collisionsCounter / pointersSize;
            }
        }
    }
    return maxProbability;
}

float RcaCryptoAnalyzer::averageFixedPoints() const {
    return ((float) totalFixedPointsCount) / (1 << automate.dimensionX);
}

float RcaCryptoAnalyzer::lightDifferentialChar() {
    unsigned int pointersSize = (1 << automate.dimensionX);
    unsigned int maskSize = (1 << MAX_LENGHT_FOR_LIGHT_DIFF_ANALYSIS);
    unsigned int maxA = 0;
    unsigned int maxB = 0;

    float maxProbability = 0;
#pragma omp parallel for
    for (unsigned int aMask = 1; aMask < maskSize; aMask++) {
        for (unsigned int shift = 0; shift < automate.dimensionX - 1; ++shift) {
            unsigned int a = aMask << shift;
            for (unsigned int bMask = 1; bMask < maskSize; bMask++) {
                for (unsigned int bShift = 0; bShift < automate.dimensionX - 1; ++bShift) {
                    unsigned int b = bMask << bShift;
                    float collisionsCounter = 0;
                    for (unsigned int x = 0; x < pointersSize; x++) {
                        automate.setStatePointer(x);
                        automate.applyFunctionStep();
                        unsigned int F_x = getCurrentStatePointer(automate.getCurrentState());

                        automate.setStatePointer(x ^ a);
                        automate.applyFunctionStep();
                        unsigned int F_x_a = getCurrentStatePointer(automate.getCurrentState());

                        if ((F_x ^ F_x_a) == b) {
                            collisionsCounter++;
                        }
                    }

#pragma omp critical
                    if (collisionsCounter / pointersSize > maxProbability) {
                        maxProbability = collisionsCounter / pointersSize;
                        maxA = a;
                        maxB = b;
                    }
                }
            }
        }
    }

    return maxProbability;
}

bool RcaCryptoAnalyzer::isCycleShifted(std::vector<unsigned int> cycle) {
    if (cycle.size() < 2) {
        return false;
    }

    int cycleBegin = cycle[0];
    for (int i = 1; i < cycle.size(); ++i) {
        if (equalWithShift(cycleBegin, cycle[i])) {
            return true;
        }
    }
    return false;
}

bool equalWithShift(unsigned int first, unsigned int second) {
    if (first == second) {
        return true;
    }

    for (unsigned int shift = second << 1; shift != second; shift = (shift << 1) | (shift >> (32 - 1))) {
        if (first == shift) return true;

    }

    return false;
}

