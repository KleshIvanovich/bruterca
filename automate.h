#pragma once
#include <vector>
#include "binaryhelper.h"

#define VON_NEYMAN_NEIGHBORHOOD 0 
#define FULL_VON_NEYMAN_NEIGHBORHOOD 1
#define ZERO_VON_NEYMAN_NEIGHBORHOOD 2
#define FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD 3

	/* Von Neyman Neighborhood NEIGHBORHOOD
	  U
	L * R
	  D
	
	Full Von Neyman Neighborhood (With Center)
	  U
	L C R
	  D
	

	*/

struct Cycle
{
	Cycle(unsigned int vertex, unsigned int length);
	unsigned int vertex;
	unsigned int length;
};

class Automate {
	public:
		Automate(unsigned int dimX,
				 unsigned int dimY,
				 unsigned int function,
				 unsigned int neighborhood,
				 unsigned int sizeOfNeighborhood,
				 bool onlyNonLinear);
		~Automate();

		int checkIfReversible();
		int writeGraphToFile(std::string pathToFile);
		int writeShiftAutomateInfo(std::string pathToFile);
		int applyFunctionStep();
	// Getters
		int getVertexSize();
		std::vector<std::vector<bool> > & getCurrentState();
		std::vector<Cycle> & getGraphStructureInfo();

		int getNewCyclePointer();
		void setStatePointer(unsigned int pointer);
        unsigned int dimensionX;
        unsigned int dimensionY;
        std::bitset<NUMBER_OF_VERTEXES> *vertexes;

	private:
	// Fields
		unsigned int function;
		unsigned int neighborhood;
		unsigned int sizeOfNeighborhood;
		int numberOfVariablesOfFunc;
		bool isShift;
		std::string formula;
		std::string typeOfFunction;
		int vertexesSize;
		unsigned int currentCyclePointer;
		unsigned int currentCycleLength;
		std::vector<std::vector<bool> > state;
		std::vector<Cycle> * currentGraphStructureInfo;
};