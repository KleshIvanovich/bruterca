#include "binaryhelper.h"
#include <iostream>
#include <iterator>
#include <algorithm>

bool calculateValue(unsigned int  function, unsigned int  vector) {
	/*
		function 1  0  0  0  1  0  0 1 0 0 1 0 0 0 0 1  16 bits
	    index   15 14 13 12 11 10  9 8 7 6 5 4 3 2 1 0
		vector < b1111
	*/
	return (!(0 == (function & (1 << vector))));
}

int getFirstNonZeroBit(std::bitset<NUMBER_OF_VERTEXES> & currentBitset, int size) {
	for (int i = 0; i < size; ++i) {
		if (currentBitset.test(i) == 0) {
			return i;
		}
	}
	return -1;
}

unsigned int  getCurrentStatePointer (std::vector<std::vector<bool>> & state) {
	/*	 0  1  2  3
		[1, 1, 0, 0] 
dimY	 4  5  6  7
		[0, 0, 0, 1]
		
			dimX
		returns b11000001 -> 193
	*/

	unsigned int result = 0;
	unsigned int shift;

	unsigned int sizeY = state.size();
	unsigned int sizeX = state[0].size();
	for (int dimY = 0; dimY < state.size(); ++dimY) {
		std::vector<bool> currentRow = state[dimY];

		for (int dimX = 0; dimX < currentRow.size(); ++dimX) {
			shift = sizeX*sizeY - 1 - (dimY*currentRow.size() + dimX);
			if (state[dimY][dimX]) {
				result = result | (1<<shift);
			}
		}
	}

	return result;
}

void setCurrentStatePointer(unsigned int number,
		std::vector<std::vector<bool>> & state) {
	unsigned int shift;

	unsigned int sizeY = state.size();
	unsigned int sizeX = state[0].size();

	for (int dimY = 0; dimY < sizeY; ++dimY) {
		std::vector<bool> currentRow = state[dimY];

		for (int dimX = 0; dimX < currentRow.size(); ++dimX) {
			shift = sizeX*sizeY - 1 - (dimY*currentRow.size() + dimX);
			state[dimY][dimX] = number & (1<<shift);
		}
	}
}

std::string stringBinaryForBoolean(unsigned int function, unsigned int numberOfVariables) {
	if (numberOfVariables == 2) {
		return std::bitset< 4 >( function ).to_string();
	}
	if (numberOfVariables == 3) {
		return std::bitset< 8 >( function ).to_string();
	}
	if (numberOfVariables == 4) {
		return std::bitset< 16 >( function ).to_string();
	}
	if (numberOfVariables == 5) {
		return std::bitset< 32 >( function ).to_string();
	}
    return std::bitset< 32 >( function ).to_string();
}

unsigned int weightForFunction(unsigned int function, unsigned int numberOfVariables) {
		if (numberOfVariables == 2) {
		return std::bitset< 4 >( function ).count();
	}
	if (numberOfVariables == 3) {
		return std::bitset< 8 >( function ).count();
	}
	if (numberOfVariables == 4) {
		return std::bitset< 16 >( function ).count();
	}
	if (numberOfVariables == 5) {
		return std::bitset< 32 >( function ).count();
	}

	return std::bitset< 32 >( function ).count();

}

std::string stringBinaryForIntegerWithLenght(unsigned int value, unsigned int length) {
	switch (length) {
		case 2:
			return std::bitset<2>(value).to_string();
		case 3:
			return std::bitset<3>(value).to_string();
		case 4:
			return std::bitset<4>(value).to_string();
		case 5:
			return std::bitset<5>(value).to_string();
		case 6:
			return std::bitset<6>(value).to_string();
		case 7:
			return std::bitset<7>(value).to_string();
		case 8:
			return std::bitset<8>(value).to_string();
		case 9:
			return std::bitset<9>(value).to_string();
		case 10:
			return std::bitset<10>(value).to_string();
		case 11:
			return std::bitset<11>(value).to_string();
		case 12:
			return std::bitset<12>(value).to_string();
		case 13:
			return std::bitset<13>(value).to_string();
		case 14:
			return std::bitset<14>(value).to_string();
		case 15:
			return std::bitset<15>(value).to_string();
		case 16:
			return std::bitset<16>(value).to_string();
		case 17:
			return std::bitset<17>(value).to_string();
		case 18:
			return std::bitset<18>(value).to_string();
		case 19:
			return std::bitset<19>(value).to_string();
		case 20:
			return std::bitset<20>(value).to_string();
		case 21:
			return std::bitset<21>(value).to_string();
		case 22:
			return std::bitset<22>(value).to_string();
		case 23:
			return std::bitset<23>(value).to_string();
		case 24:
			return std::bitset<24>(value).to_string();
		case 25:
			return std::bitset<25>(value).to_string();
		case 26:
			return std::bitset<26>(value).to_string();
		case 27:
			return std::bitset<27>(value).to_string();
		case 28:
			return std::bitset<28>(value).to_string();
		case 29:
			return std::bitset<29>(value).to_string();
		case 30:
			return std::bitset<30>(value).to_string();
		case 31:
			return std::bitset<30>(value).to_string();
		default:
			return std::bitset<32>(value).to_string();
	}
}

std::vector<bool> ghegalkin(unsigned int function, unsigned int numberOfVariables) {
	/*
		1. Reverse function
		2. Recursive calculate value in depence of 

		0000 1 1 0 0 0 0 1 1 0 0 1 1 1 1 0 0
		0001  0 1 0 0 0 1 0 1 0 1 0 0 0 1 0
		0010   1 1 0 0 1 1 1 1 1 1 0 0 1 1
		0011    0 1 0 1 0 0 0 0 0 1 0 1 0
		0100     1 1 1 1 0 0 0 0 1 1 1 1
		0101      0 0 0 1 0 0 0 1 0 0 0
		0110       0 0 1 1 0 0 1 1 0 0
		0111	    0 1 0 1 0 1 0 1 0
		1000         1 1 1 1 1 1 1 1
		1000	      0 0 0 0 0 0 0 
		1001	       0 0 0 0 0 0
	*/

	// (2^n)
	unsigned int lengthOfValueVector = (1 << numberOfVariables);

	std::vector<std::vector<bool>> vectorOfFunction(1);
	std::vector<bool> innerVector(lengthOfValueVector);
	innerVector.resize(lengthOfValueVector);
	vectorOfFunction[0] = innerVector;

	setCurrentStatePointer(function, vectorOfFunction);

	std::vector<bool> trueVectorFunction = toReverseOrder(
										integerToBooleanVector(
			getCurrentStatePointer(vectorOfFunction), lengthOfValueVector));

	std::vector<bool> polynomCoef;
	polynomCoef.resize((lengthOfValueVector));

	std::vector<bool> currentVector = trueVectorFunction;
	polynomCoef[0] = currentVector[0];

	for (int i = 0; i < trueVectorFunction.size() - 1; ++i) {
		std::vector<bool> temporaryVector;
		temporaryVector.resize(currentVector.size() - 1);
		for (int j = 0; j < temporaryVector.size(); ++j) {
			temporaryVector[j] = currentVector[j]^currentVector[j + 1];
		}
		polynomCoef[i + 1] = temporaryVector[0];
		currentVector = std::vector<bool>(temporaryVector);
	}

	return polynomCoef;
}

std::string getAnfDescription(std::vector<bool> & polynomCoef, int numberOfVariables) {
	std::string result;
	for (int iter = 0; iter < polynomCoef.size(); ++iter) {
		result = result + stringBinaryForIntegerWithLenght(iter, numberOfVariables)
		 + " " + (polynomCoef[iter] == true ? "1" : "0")  + "\r\n";
	}
	return result;
}

std::vector<bool> integerToBooleanVector(unsigned int value, unsigned int size) {
	std::vector<bool> currentRow;
	currentRow.resize(size);
	unsigned int shift;

		for (int dimX = 0; dimX < size; ++dimX) {
			shift = size - 1 - dimX;
			currentRow[dimX] = value & (1<<shift);
		}

		toReverseOrder(currentRow);
		return currentRow;
}

std::vector<bool> toReverseOrder(std::vector<bool> source) {
	std::vector<bool> destination;
	unsigned int size = source.size();
	destination.resize(source.size());
	for (int i = 0; i < source.size(); ++i) {
		destination[i] = source[size - i - 1];
	}
	return destination;
}

Function getFormulaFromAnf(std::vector<bool> & anf, std::vector<std::string> & literals) {
	/*
		1. Need to know how many coefficients
		2. Get coefficient, and need to calculate string representation for him
			2.1 In depence of neighborhood and size - we get different rule
			2.2 Let's keep an array with literals. For example, if we have function of 3 variables,
				then array will be: ["L", "C", "R"]
			2.3 Get string binary for coefficient, and replace it with multiplication of coefficients
			2.4 If coefficient is not last - put "+" symbol after. 
	*/
	int countOfCoefficients = 0;

	int nonLinear = 1 << 1;
	int inverse = 1;
	int typeFlags = 0;

	for (int iter = 0; iter < anf.size(); ++iter) {
		if (anf[iter]) countOfCoefficients++;
	}

	std::string formula = "";
	std::string trueValueString = "1";
	char trueValue = trueValueString[0];
	int handledCoefs = 0;

	for (int iter = 0; iter < anf.size(); ++iter) {
		if (anf[iter]==false) {
			continue;
		}


		int weightForCoef = std::bitset< 8 >(iter).count();

		if (weightForCoef > 1) {
			typeFlags = typeFlags | nonLinear;
		}

		std::string currentCoef =  stringBinaryForIntegerWithLenght(iter, literals.size());
		std::string literal = "";


		for (int symbol = 0; symbol < currentCoef.size(); ++symbol) {
			char currentSymbol = currentCoef[symbol];
			if (currentSymbol == trueValue) {
				literal = literal + literals[symbol];
				if (symbol >= currentCoef.size() - 1) {
					break;
				}

				bool needToMultiply = false;
				for (int nextSymbols = symbol + 1; nextSymbols < currentCoef.size(); nextSymbols++) {
					if (currentCoef[nextSymbols] == trueValue) {
						needToMultiply = true;
					}
				}

				if (needToMultiply) {
					literal = literal + "*";
				}
			}
		}

		if (literal == "") {
			typeFlags = typeFlags | inverse;
			literal = "1";
		} 

		formula = formula + literal;

		handledCoefs++;

		if (handledCoefs == countOfCoefficients ) {
			break;
		}
		formula = formula + " + ";
	}

	std::string typeOfFunc = getTypeOfFunctionFromMask(typeFlags, countOfCoefficients);
	Function func(formula, typeOfFunc);
	return func;
}

std::string getTypeOfFunctionFromMask(int mask, int countOfCoefs) {
	if (mask == 0 || mask == 1) {
	
		if ((mask == 1 && countOfCoefs <= 2) || 
			(mask == 0 && countOfCoefs <= 1)) {
			std::string type = "Shift";
			return type;
		}

		std::string type = "LinearNotShift";
		return type;
	}

	std::string type = "NonLinear";
	return type;
}

Function::Function(std::string & formula, std::string & typeOfFunction){
	this->formula = formula;
	this->typeOfFunction = typeOfFunction;
}
	
std::string & Function::getFormula() {
	return formula;
}
std::string & Function::getTypeOfFunction() {
	return typeOfFunction;
}

