#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <cmath>
#include "automate.h"
#include "rca_crypto_analyser.h"

std::string getStringFromNeighborhood(int neighborhood);

int getSizeOfNeighborhood(const std::string &function);

int getNeighborhoodForFunction(const std::string &function);

std::vector<std::string> literalsForNeighborhood(int neighborhood, int dimY, int size);

std::vector<std::string> split(const std::string &s, char delim);


int main(int argc, char *argv[]) {

    if (argc < 2) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " function size" << std::endl;
        std::cerr << "Example:" << std::endl;
        std::cerr << argv[0] << " 0011001110010011" << " 7" << std::endl;
        return 1;
    }

    int length;
    std::string stringFunction;

    if (argc == 3) {
        length = std::stoi(argv[2], 0, 10); // 7;
        stringFunction = argv[1]; // "0011001110010011";
    } else {
        std::vector<std::string> arguments = split(argv[1], ' ');
        length = std::stoi(arguments[1], 0, 10); // 7;
        stringFunction = arguments[0]; // "0011001110010011";
    }

    unsigned int function = std::stol(stringFunction, 0, 2);
    int neighborhood = getNeighborhoodForFunction(stringFunction);
    std::string stringNeighborhood = getStringFromNeighborhood(neighborhood);
    int sizeOfNeighborhood = getSizeOfNeighborhood(stringFunction);

    int numberOfVariables = log2(stringFunction.length());

    Automate *automate = new Automate(
            length,
            1,
            function,
            neighborhood,
            sizeOfNeighborhood,
            false
    );

    if (automate->checkIfReversible() != 0) {
        return 0;
    } else {
        automate = new Automate(
                length,
                1,
                function,
                neighborhood,
                sizeOfNeighborhood,
                false
        );
    }

    auto *analyzer = new RcaCryptoAnalyzer(*automate);
    float lightDiffChar = length < 11 ? analyzer->differentialChar() : analyzer->lightDifferentialChar();
    auto ghegalkinCoef = ghegalkin(function, numberOfVariables);
    auto literals = literalsForNeighborhood(neighborhood, 1, sizeOfNeighborhood);
    auto formula = getFormulaFromAnf(ghegalkinCoef, literals).getFormula();
    std::cout <<
              "|" << stringFunction <<
              "|" << formula <<
              "|" << length <<
              "|" << getStringFromNeighborhood(neighborhood) <<
              "|" << sizeOfNeighborhood <<
              "|" << analyzer->averageCycleLength() <<
              "|" << analyzer->averageFixedPoints() <<
              "|" << analyzer->shiftedCyclesCount() <<
              "|" << analyzer->shiftedCyclesLength() <<
              "|" << lightDiffChar <<
              "|" << std::endl;

    return 0;
}

std::string getStringFromNeighborhood(int neighborhood) {
    if (neighborhood == VON_NEYMAN_NEIGHBORHOOD) {
        return "VON_NEYMAN_NEIGHBORHOOD";
    }
    if (neighborhood == FULL_VON_NEYMAN_NEIGHBORHOOD) {
        return "FULL_VON_NEYMAN_NEIGHBORHOOD";
    }
    return "VON_NEYMAN_NEIGHBORHOOD";
}

int getNeighborhoodForFunction(const std::string &function) {
    int functionLength = function.length();

    switch (functionLength) {
        case 8:
            return FULL_VON_NEYMAN_NEIGHBORHOOD;
        case 16:
            return VON_NEYMAN_NEIGHBORHOOD;
        case 32:
            return FULL_VON_NEYMAN_NEIGHBORHOOD;
        default:
            return FULL_VON_NEYMAN_NEIGHBORHOOD;
    }
}

int getSizeOfNeighborhood(const std::string &function) {
    int functionLength = function.length();

    switch (functionLength) {
        case 8:
            return 1;
        case 16:
            return 2;
        case 32:
            return 2;
        default:
            return 2;
    }
}

std::vector<std::string> literalsForNeighborhood(int neighborhood, int dimY, int size) {
    if (neighborhood == VON_NEYMAN_NEIGHBORHOOD || neighborhood == ZERO_VON_NEYMAN_NEIGHBORHOOD) {
        if (dimY == 1) {
            if (size == 1) {
                std::vector<std::string> result{"L", "R"};
                return result;
            }
            if (size == 2) {
                std::vector<std::string> result{"L2", "L1", "R1", "R2"};
                return result;
            }
        }
        std::vector<std::string> result = {"U", "R", "L", "D"};
        return result;
    }

    if (neighborhood == FULL_VON_NEYMAN_NEIGHBORHOOD || neighborhood == FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD) {
        if (dimY == 1) {
            if (size == 1) {
                std::vector<std::string> result{"L", "C", "R"};
                return result;
            }
            if (size == 2) {
                std::vector<std::string> result{"L2", "L1", "C", "R1", "R2"};
                return result;
            }
        }

        std::vector<std::string> result = {"U", "R", "D", "L", "C"};
        return result;
    }

    return {};
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}