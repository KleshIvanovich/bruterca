#include "automate.h"
#include <iostream>
#include <fstream>

std::string getDescriptionForNeighborhood(int neighborhood, int dimY, int size);

std::string getFilePath(std::string binaryFunction, std::string parent, std::string typeOfFunction,
                        int neighborhood, int size, int dimX, int dimY);

std::vector<std::string> getLiteralsForNeighborhood(int neighborhood, int dimY, int size);

std::string getPathForNeighborhood(int neighborhood);


Automate::Automate(unsigned int dimX,
                   unsigned int dimY,
                   unsigned int function,
                   unsigned int neighborhood,
                   unsigned int sizeOfNeighborhood,
                   bool onlyNonLinear) {


    this->dimensionY = dimY;
    this->dimensionX = dimX;
    this->function = function;
    this->neighborhood = neighborhood;
    this->sizeOfNeighborhood = sizeOfNeighborhood;

    if ((this->neighborhood == VON_NEYMAN_NEIGHBORHOOD) || (this->neighborhood == ZERO_VON_NEYMAN_NEIGHBORHOOD)) {
        if (this->dimensionY == 1) {
            if (sizeOfNeighborhood == 1) {
                this->numberOfVariablesOfFunc = 2;
            }
            if (this->sizeOfNeighborhood == 2) {
                this->numberOfVariablesOfFunc = 4;
            }
        } else {
            this->numberOfVariablesOfFunc = 4;
        }
    }

    if ((this->neighborhood == FULL_VON_NEYMAN_NEIGHBORHOOD) ||
        (this->neighborhood == FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD)) {
        if (this->dimensionY == 1) {
            if (this->sizeOfNeighborhood == 1) {
                this->numberOfVariablesOfFunc = 3;
            }
            if (this->sizeOfNeighborhood == 2) {
                this->numberOfVariablesOfFunc = 5;
            }
        } else {
            this->numberOfVariablesOfFunc = 5;
        }

    }

    std::vector<bool> anfPolynomCoef = ghegalkin(this->function, this->numberOfVariablesOfFunc);
    std::vector<std::string> literals = getLiteralsForNeighborhood(
            this->neighborhood, this->dimensionY, this->sizeOfNeighborhood);

    this->formula = getFormulaFromAnf(anfPolynomCoef, literals).getFormula();
    this->typeOfFunction = getFormulaFromAnf(anfPolynomCoef, literals).getTypeOfFunction();

    this->typeOfFunction = getFormulaFromAnf(anfPolynomCoef, literals).getTypeOfFunction();
    this->isShift = ((this->typeOfFunction == "Shift") ||
                     (this->typeOfFunction == "LinearNotShift")) && onlyNonLinear;

    if (this->isShift) {
        return;
    }
    // dimX*dimY <=24
    this->vertexes = new std::bitset<NUMBER_OF_VERTEXES>(0);
    this->vertexesSize = 1 << (dimX * dimY);
    this->currentCyclePointer = 0;
    this->currentCycleLength = 0;
    this->currentGraphStructureInfo = new std::vector<Cycle>();

    std::vector<std::vector<bool> > istate(dimY);
    // Zero init
    for (size_t i = 0; i < dimY; ++i) {
        istate[i].resize(dimX);
        for (size_t j = 0; j < dimX; ++j) {
            istate[i][j] = false;
        }
    }

    this->state = istate;
}

Automate::~Automate() {
    if (this->isShift) {
        return;
    }
    delete this->vertexes;
    delete this->currentGraphStructureInfo;
}

Cycle::Cycle(unsigned int vertex, unsigned int length) {
    this->vertex = vertex;
    this->length = length;
}

int Automate::checkIfReversible() {
    /*

    1. Need to keep only bitset of quantity of graph vertexes ~ 2^(dimX*dimY -3) bytes
    2. Each time check - was this vertex (bit with number of vertex) seen previously?
        2.1 If no, get next vertex by applying function (applyFunctionStep), go to p.2
    3. If yes, was it beginning of cycle?
        3.1 If no - automate is inreversible, return -1
        3.2 If Yes - save to another array structure with this vertex as beginning of cycle, and it's length
            3.3 If new cycle pointer is end of bitset - automate is reversible, return 0;
    4. Get first zero-bit from bitset, take it as a new vertex of new cycle, go to p.2

     */
    unsigned int currentState;

    if (isShift) {
        return 0;
    }

    while (true) {
        currentState = getCurrentStatePointer(this->state);
        if (this->vertexes->test(currentState) == 0) {
            this->vertexes->set(currentState);
            applyFunctionStep();
            currentCycleLength++;
            continue;
        }

        if (currentState != this->currentCyclePointer) {
            return -1;
        }

        Cycle cycleToSave(this->currentCyclePointer, this->currentCycleLength);
        currentGraphStructureInfo->push_back(cycleToSave);

        int newCyclePointer = getFirstNonZeroBit(*(this->vertexes), this->vertexesSize);
        if (newCyclePointer == -1) {
            return 0;
        }

        this->currentCyclePointer = getFirstNonZeroBit(*(this->vertexes), this->vertexesSize);
        setCurrentStatePointer(this->currentCyclePointer, this->state);

        this->currentCycleLength = 0;
    }

    return 0;
}

// Getters

int Automate::getVertexSize() {
    return this->vertexes->size();
}

std::vector<std::vector<bool> > &Automate::getCurrentState() {
    return this->state;
}

std::vector<Cycle> &Automate::getGraphStructureInfo() {
    return *(this->currentGraphStructureInfo);
}

void Automate::setStatePointer(unsigned int pointer) {
    setCurrentStatePointer(pointer, this->state);
}

int Automate::applyFunctionStep() {
    /* Transform each cell of state with function
      U
    L * R
      D

      U
    L C R
      D

      F(U, R, D, L)
      F(U, R, D, L, C)

    */

    unsigned int sizeY = dimensionY;
    unsigned int sizeX = dimensionX;

    std::vector<std::vector<bool> > result = std::vector<std::vector<bool> >();

    for (unsigned int dimY = 0; dimY < sizeY; ++dimY) {
        std::vector<bool> currentRow = std::vector<bool>(sizeX);

        for (unsigned int dimX = 0; dimX < sizeX; ++dimX) {

            if (this->neighborhood == VON_NEYMAN_NEIGHBORHOOD) {
                bool left = state[dimY][(dimX - 1) % sizeX];
                bool right = state[dimY][(dimX + 1) % sizeX];


                if (sizeY == 1) {
                    if (sizeOfNeighborhood == 1) {
                        // |*|x|*|
                        std::vector<bool> toVector = {left, right};
                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);

                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }

                    if (sizeOfNeighborhood == 2) {
                        // |*|*|x|*|*|
                        bool leftleft = state[dimY][(dimX - 2) % sizeX];
                        bool rightright = state[dimY][(dimX + 2) % sizeX];

                        std::vector<bool> toVector = {leftleft,
                                                      left,
                                                      right,
                                                      rightright};
                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);

                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }
                }


                /*
                   |-|*|-|
                   |*|x|*|
                   |-|*|-|
                */

                bool up = state[(dimY - 1) % sizeY][dimX];
                bool down = state[(dimY + 1) % sizeY][dimX];

                if (sizeOfNeighborhood == 1) {
                    // |*|x|*|
                    std::vector<bool> toVector = {up, right, down, left};

                    std::vector<std::vector<bool> > toMatrix = {toVector};
                    unsigned int vector = getCurrentStatePointer(toMatrix);

                    currentRow[dimX] = calculateValue(this->function, vector);
                    continue;
                }
            }

            if (this->neighborhood == FULL_VON_NEYMAN_NEIGHBORHOOD) {
                bool left = state[dimY][(dimX - 1) % sizeX];
                bool right = state[dimY][(dimX + 1) % sizeX];
                bool current = state[dimY][dimX];
                if (sizeY == 1) {
                    if (sizeOfNeighborhood == 1) {
                        // |*|x*|*|
                        std::vector<bool> toVector = {left, current, right};
                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);

                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }

                    if (sizeOfNeighborhood == 2) {
                        // |*|*|x*|*|*|
                        bool leftleft = state[dimY][(dimX - 2) % sizeX];
                        bool rightright = state[dimY][(dimX + 2) % sizeX];

                        std::vector<bool> toVector = {leftleft,
                                                      left,
                                                      current,
                                                      right,
                                                      rightright};

                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);

                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }
                }
                /*
                   |-|*|-|
                   |*|*|*|
                   |-|*|-|

                */
                bool up = state[(dimY - 1) % sizeY][dimX];
                bool down = state[(dimY + 1) % sizeY][dimX];

                if (sizeOfNeighborhood == 1) {
                    std::vector<bool> toVector = {up, right, down, left, current};
                    std::vector<std::vector<bool> > toMatrix = {toVector};
                    unsigned int vector = getCurrentStatePointer(toMatrix);

                    currentRow[dimX] = calculateValue(this->function, vector);
                    continue;
                }
            }
            if (this->neighborhood == ZERO_VON_NEYMAN_NEIGHBORHOOD) {
                bool left = false;

                if (dimX > 0) {
                    left = state[dimY][dimX - 1];
                }

                bool right = false;
                if ((dimX + 1) < dimensionX) {
                    right = state[dimY][dimX + 1];
                }

                if (sizeY == 1) {
                    if (sizeOfNeighborhood == 1) {
                        // |*|x|*|
                        std::vector<bool> toVector = {left, right};
                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);
                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }

                    if (sizeOfNeighborhood == 2) {
                        // |*|*|x|*|*|
                        bool leftleft = false;

                        if (dimX > 1) {
                            leftleft = state[dimY][(dimX - 2)];
                        }

                        bool rightright = false;
                        if (dimX + 2 < dimensionX) {
                            rightright = state[dimY][(dimX + 2)];
                        }

                        std::vector<bool> toVector = {leftleft,
                                                      left,
                                                      right,
                                                      rightright};

                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);

                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }
                }

                bool up = false;
                if (dimY > 0) {
                    up = state[(dimY - 1)][dimX];
                }

                bool down = false;
                if (dimY + 1 < dimensionY) {
                    down = state[(dimY + 1)][dimX];
                }

                if (sizeOfNeighborhood == 1) {
                    // |*|x|*|
                    std::vector<bool> toVector = {up, right, down, left};
                    std::vector<std::vector<bool> > toMatrix = {toVector};
                    unsigned int vector = getCurrentStatePointer(toMatrix);

                    currentRow[dimX] = calculateValue(this->function, vector);
                    continue;
                }
            }
            if (this->neighborhood == FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD) {
                bool left = false;

                if (dimX > 0) {
                    left = state[dimY][dimX - 1];
                }

                bool right = false;
                if ((dimX + 1) < dimensionX) {
                    right = state[dimY][dimX + 1];
                }

                bool center = state[dimY][dimX];

                if (sizeY == 1) {
                    if (sizeOfNeighborhood == 1) {
                        // |*|x*|*|
                        std::vector<bool> toVector = {left, center, right};
                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);
                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }

                    if (sizeOfNeighborhood == 2) {
                        // |*|*|x*|*|*|
                        bool leftleft = false;
                        if (dimX - 2 >= 0 && (dimX - 2 < dimensionX)) {
                            leftleft = state[dimY][(dimX - 2)];
                        }

                        bool rightright = false;
                        if (dimX + 2 <= dimensionX) {
                            rightright = state[dimY][(dimX + 2)];
                        }

                        std::vector<bool> toVector = {leftleft,
                                                      left,
                                                      center,
                                                      right,
                                                      rightright};
                        std::vector<std::vector<bool> > toMatrix = {toVector};
                        unsigned int vector = getCurrentStatePointer(toMatrix);

                        currentRow[dimX] = calculateValue(this->function, vector);
                        continue;
                    }
                }

                bool up = false;
                if (dimY > 0) {
                    up = state[(dimY - 1)][dimX];
                }

                bool down = false;
                if (dimY + 1 < dimensionY) {
                    down = state[(dimY + 1)][dimX];
                }


                if (sizeOfNeighborhood == 1) {
                    std::vector<bool> toVector = {up, right, down, left, center};
                    std::vector<std::vector<bool> > toMatrix = {toVector};
                    unsigned int vector = getCurrentStatePointer(toMatrix);

                    currentRow[dimX] = calculateValue(this->function, vector);
                    continue;
                }
            }
        }

        result.push_back(currentRow);

    }

    this->state = result;
    return 0;
}


int Automate::writeShiftAutomateInfo(std::string parentDirectory) {
    std::vector<bool> anfPolynomCoef = ghegalkin(this->function, numberOfVariablesOfFunc);

    std::string filepath = getFilePath(stringBinaryForBoolean(this->function, numberOfVariablesOfFunc),
                                       parentDirectory, this->typeOfFunction,
                                       neighborhood, sizeOfNeighborhood,
                                       dimensionX, dimensionY);

    std::ofstream fout(filepath + "/" + this->formula + ".txt");

    fout << "dimX:" << this->dimensionX << " dimY:" << this->dimensionY << "\r\n";
    fout << "2^n ... ... ... ... ... ... 0" << "\r\n";
    fout << "function:" << stringBinaryForBoolean(this->function, numberOfVariablesOfFunc)
         <<
         " with weight:" << weightForFunction(function, numberOfVariablesOfFunc)
         << "\r\n"
         << this->formula << "\r\n"
         << getDescriptionForNeighborhood(neighborhood, dimensionY, sizeOfNeighborhood) << "\r\n"
         << "ANF:" << "\r\n"
         << getAnfDescription(anfPolynomCoef, numberOfVariablesOfFunc) << "\r\n";
    return 0;
}


int Automate::writeGraphToFile(std::string parentDirectory) {
    if (isShift) {
//        std::cout << "SHIFT" << std::endl;
//        writeShiftAutomateInfo(parentDirectory);
        return 0;
    }

    std::vector<bool> anfPolynomCoef = ghegalkin(this->function, numberOfVariablesOfFunc);

    int totalLength = 0;


    std::string filepath = getFilePath(stringBinaryForBoolean(this->function, numberOfVariablesOfFunc),
                                       parentDirectory, this->typeOfFunction,
                                       neighborhood, sizeOfNeighborhood,
                                       dimensionX, dimensionY);


    std::ofstream fout(filepath + ".txt");

    fout << "dimX:" << this->dimensionX << " dimY:" << this->dimensionY << "\r\n";
    fout << "2^n ... ... ... ... ... ... 0" << "\r\n";
    fout << "function:" << stringBinaryForBoolean(this->function, numberOfVariablesOfFunc)
         <<
         " with weight:" << weightForFunction(function, numberOfVariablesOfFunc)
         << "\r\n"
         << this->formula << "\r\n"
         << getDescriptionForNeighborhood(neighborhood, dimensionY, sizeOfNeighborhood) << "\r\n"
         << "ANF:" << "\r\n"
         << getAnfDescription(anfPolynomCoef, numberOfVariablesOfFunc) << "\r\n";
    for (int i = 0; i < this->currentGraphStructureInfo->size(); ++i) {
        totalLength += (*currentGraphStructureInfo)[i].length;
        fout << "Cycle " << i << " vertex:" << (*currentGraphStructureInfo)[i].vertex
             << " " << stringBinaryForIntegerWithLenght((*currentGraphStructureInfo)[i].vertex, dimensionX * dimensionY)
             << " length:" << (*currentGraphStructureInfo)[i].length << "\r\n";
    }

    fout << "Total Length:" << totalLength << "\r\n";
    return 0;
}

int Automate::getNewCyclePointer() {
    return getFirstNonZeroBit(*(this->vertexes), this->vertexesSize);
}

std::string getDescriptionForNeighborhood(int neighborhood, int dimY, int size) {
    if (neighborhood == VON_NEYMAN_NEIGHBORHOOD || neighborhood == ZERO_VON_NEYMAN_NEIGHBORHOOD) {
        if (dimY == 1) {
            if (size == 1) {
                return std::string("|L|x|R| F(L,R)");
            }
            if (size == 2) {
                return std::string("|L2|L1|x|R1|R2| F(L2, L1, R1, R2)");
            }
        }
        std::string result = std::string("| |U| |") + "\r\n" +
                             "|L|x|R|" + "\r\n" +
                             "| |D| |" + "\r\n" +
                             "F(U,R,L,D)" + "\r\n";
        return result;
    }

    if (neighborhood == FULL_VON_NEYMAN_NEIGHBORHOOD || neighborhood == FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD) {
        if (dimY == 1) {
            if (size == 1) {
                return std::string("|L|C|R| F(L,C,R)");
            }
            if (size == 2) {
                return std::string("|L2|L1|C|R1|R2| F(L2, L1, C, R1, R2)");
            }
        }

        std::string result = std::string("| |U| |") + "\r\n" +
                             "|L|C|R|" + "\r\n" +
                             "| |D| |" + "\r\n" +
                             "F(U,R,L,D,C)" + "\r\n";
    }

    return std::string("");
}

std::vector<std::string> getLiteralsForNeighborhood(int neighborhood, int dimY, int size) {
    if (neighborhood == VON_NEYMAN_NEIGHBORHOOD || neighborhood == ZERO_VON_NEYMAN_NEIGHBORHOOD) {
        if (dimY == 1) {
            if (size == 1) {
                std::vector<std::string> result{"L", "R"};
                return result;
            }
            if (size == 2) {
                std::vector<std::string> result{"L2", "L1", "R1", "R2"};
                return result;
            }
        }
        std::vector<std::string> result = {"U", "R", "L", "D"};
        return result;
    }

    if (neighborhood == FULL_VON_NEYMAN_NEIGHBORHOOD || neighborhood == FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD) {
        if (dimY == 1) {
            if (size == 1) {
                std::vector<std::string> result{"L", "C", "R"};
                return result;
            }
            if (size == 2) {
                std::vector<std::string> result{"L2", "L1", "C", "R1", "R2"};
                return result;
            }
        }

        std::vector<std::string> result = {"U", "R", "D", "L", "C"};
        return result;
    }

    return {};
}

std::string getFilePath(std::string binaryFunction, std::string parent, std::string typeOfFunction,
                        int neighborhood, int size, int dimX, int dimY) {
    std::string directoryForDimension = parent + "/" + binaryFunction;
    system(("mkdir -p " + directoryForDimension).c_str());

    directoryForDimension = directoryForDimension + "/"
                            + getPathForNeighborhood(neighborhood) + "_size_" + std::to_string(size) + "_" +
                            std::to_string(dimX) + "x" + std::to_string(dimY);
    return directoryForDimension;

}


std::string getPathForNeighborhood(int neighborhood) {
    switch (neighborhood) {
        case VON_NEYMAN_NEIGHBORHOOD:
            return "VonNeymanNeighborhood";
        case FULL_VON_NEYMAN_NEIGHBORHOOD:
            return "FullVonNeymanNeighborhood";
        case ZERO_VON_NEYMAN_NEIGHBORHOOD:
            return "ZeroVonNeymanNeighborhood";
        case FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD:
            return "FullZeroVonNeymanNeighborhood";
    }

    return "";
}




