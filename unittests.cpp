#include "gtest/gtest.h"
#include "automate.h"
#include "binaryhelper.h"
#include "rca_crypto_analyser.h"
#include <iostream>

namespace {

// The fixture for testing class Foo.
    class AutomateTests : public ::testing::Test {
    protected:
        // You can remove any or all of the following functions if its body
        // is empty.

        AutomateTests() {
            // You can do set-up work for each test here.
        }

        ~AutomateTests() override {
            // You can do clean-up work that doesn't throw exceptions here.
        }


        void SetUp() override {

        }

        void TearDown() override {

        }

    };

    TEST_F(AutomateTests, TestConstructorCorrect) {

        const int dimY = 4;
        const int dimX = 4;

        Automate *automate = new Automate(dimX, dimY, 4, VON_NEYMAN_NEIGHBORHOOD, 1, false);
        EXPECT_EQ(automate->getVertexSize(), ((1 << 25) - 1));
        std::vector<std::vector<bool>> state = automate->getCurrentState();

        EXPECT_EQ(0, getCurrentStatePointer(state));
        EXPECT_EQ(dimY, state.size());
        EXPECT_EQ(dimX, state[0].size());

        std::vector<Cycle> graphStructure = automate->getGraphStructureInfo();
        Cycle *cycle = new Cycle(0, 0);
        graphStructure.push_back(*cycle);
        EXPECT_EQ(1, graphStructure.size());

    }

    TEST_F(AutomateTests, testApplyStep) {
        const int dimY = 1;
        const int dimX = 8;     //3 2 1 0
        const int function = 5; //0 1 0 1

        Automate *automate = new Automate(dimX, dimY, 5, VON_NEYMAN_NEIGHBORHOOD, 1, false);
        std::vector<std::vector<bool>> fullTestState = automate->getCurrentState();
        EXPECT_EQ(automate->getVertexSize(), ((1 << 25) - 1));
        EXPECT_EQ(0, getCurrentStatePointer(fullTestState));
        automate->applyFunctionStep();
        fullTestState = automate->getCurrentState();
        // 00000000 -> 11111111
        EXPECT_EQ(255, getCurrentStatePointer(fullTestState));
        // 11111111 -> 00000000
        automate->applyFunctionStep();
        fullTestState = automate->getCurrentState();
        EXPECT_EQ(0, getCurrentStatePointer(fullTestState));
        // 11101011
    }

    TEST_F(AutomateTests, testApplyStep2) {
        const int dimY = 1;
        const int dimX = 8;     //3 2 1 0
        const int function = 5; //0 1 0 1

        Automate *automate = new Automate(dimX, dimY, 5, VON_NEYMAN_NEIGHBORHOOD, 1, false);
        std::cout << "Constructed" << std::endl;
        std::vector<std::vector<bool>> fullTestState = automate->getCurrentState();
        // 11101011
        std::cout << "Test State" << std::endl;
        automate->setStatePointer(232);
        fullTestState = automate->getCurrentState();
        EXPECT_EQ(232, getCurrentStatePointer(fullTestState));
        std::cout << "Test State 2" << std::endl;
        automate->applyFunctionStep();
        fullTestState = automate->getCurrentState();
        std::cout << "CurrentState " << getCurrentStatePointer(fullTestState) << std::endl;
        EXPECT_EQ(46, getCurrentStatePointer(fullTestState));
    }

    TEST_F(AutomateTests, testApplyStep3) {
        const int dimY = 2;
        const int dimX = 4;
        const int function = 23117; // 0101101001001101

        Automate *automate = new Automate(dimX, dimY, function, VON_NEYMAN_NEIGHBORHOOD, 1, false);
        std::vector<std::vector<bool>> fullTestState = automate->getCurrentState();
        // 11101011
        automate->setStatePointer(235);
        fullTestState = automate->getCurrentState();
        EXPECT_EQ(235, getCurrentStatePointer(fullTestState));

        automate->applyFunctionStep();
        fullTestState = automate->getCurrentState();
        std::cout << "CurrentState " << getCurrentStatePointer(fullTestState) << std::endl;
        EXPECT_EQ(170, getCurrentStatePointer(fullTestState));
    }

    TEST_F(AutomateTests, testApplyStep4) {
        const int dimY = 2;
        const int dimX = 4;
        const unsigned int function = (1 << 31) + 23117; //1000000000000000 0101101001001101

        Automate *automate = new Automate(dimX, dimY, function, FULL_VON_NEYMAN_NEIGHBORHOOD, 1, false);
        std::vector<std::vector<bool>> fullTestState = automate->getCurrentState();
        // 11101011
        automate->setStatePointer(235);
        fullTestState = automate->getCurrentState();
        EXPECT_EQ(235, getCurrentStatePointer(fullTestState));

        automate->applyFunctionStep();
        fullTestState = automate->getCurrentState();
        std::cout << "CurrentState " << getCurrentStatePointer(fullTestState) << std::endl;
        EXPECT_EQ(65, getCurrentStatePointer(fullTestState));
    }

    TEST_F(AutomateTests, testApplyStep5) {
        const int dimY = 2;
        const int dimX = 4;
        const int function = 23117; // 0101101001001101

        Automate *automate = new Automate(dimX, dimY, function, ZERO_VON_NEYMAN_NEIGHBORHOOD, 1, false);
        std::vector<std::vector<bool>> fullTestState = automate->getCurrentState();
        // 11101011
        automate->setStatePointer(235);
        fullTestState = automate->getCurrentState();
        EXPECT_EQ(235, getCurrentStatePointer(fullTestState));

        automate->applyFunctionStep();
        fullTestState = automate->getCurrentState();
        std::cout << "CurrentState " << getCurrentStatePointer(fullTestState) << std::endl;
        EXPECT_EQ(178, getCurrentStatePointer(fullTestState));
    }

    TEST_F(AutomateTests, testApplyStep6) {
        const int dimY = 2;
        const int dimX = 4;
        const int function = (1 << 31) + 23117; // 0101101001001101

        Automate *automate = new Automate(dimX, dimY, function, FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD, 1, false);
        std::vector<std::vector<bool>> fullTestState = automate->getCurrentState();
        // 11101011
        automate->setStatePointer(235);
        fullTestState = automate->getCurrentState();
        EXPECT_EQ(235, getCurrentStatePointer(fullTestState));

        automate->applyFunctionStep();
        fullTestState = automate->getCurrentState();
        std::cout << "CurrentState " << getCurrentStatePointer(fullTestState) << std::endl;
        EXPECT_EQ(81, getCurrentStatePointer(fullTestState));
    }

    TEST_F(AutomateTests, BinaryHelperTests) {
        int function = 14; // 1110
        EXPECT_EQ(calculateValue(function, 0), false);
        EXPECT_EQ(calculateValue(function, 1), true);
        EXPECT_EQ(calculateValue(function, 2), true);
        EXPECT_EQ(calculateValue(function, 3), true);

        function = 232; // 11101000

        EXPECT_EQ(calculateValue(function, 0), false);
        EXPECT_EQ(calculateValue(function, 1), false);
        EXPECT_EQ(calculateValue(function, 2), false);
        EXPECT_EQ(calculateValue(function, 3), true);
        EXPECT_EQ(calculateValue(function, 4), false);
        EXPECT_EQ(calculateValue(function, 5), true);
        EXPECT_EQ(calculateValue(function, 6), true);
        EXPECT_EQ(calculateValue(function, 7), true);
        //1 1 0 0 0 0 1 1 0 0 1 1 1 1 0 0
        ghegalkin(15555, 4);
        // 10010110
        std::vector<bool> anf1 = ghegalkin(150, 3);
        std::cout << getAnfDescription(anf1, 3);
        // 1001
        std::vector<bool> anf2 = ghegalkin(9, 2);
        std::cout << getAnfDescription(anf2, 2);
    }

    TEST_F(AutomateTests, getStatePointerMatrix2) {
        /*   0  1  2  3
        [1, 1, 0, 0]
    dimY   4  5  6  7
        [0, 0, 0, 1]

          dimX
        returns b11000001 -> 193
      */

        std::vector<bool> testRow1 = {true, true, false, false};
        std::vector<bool> testRow2 = {false, false, false, true};

        std::vector<std::vector<bool>> fullTestState = {testRow1, testRow2};

        EXPECT_EQ(193, getCurrentStatePointer(fullTestState));

        setCurrentStatePointer(200, fullTestState);
        EXPECT_EQ(200, getCurrentStatePointer(fullTestState));

    }

    TEST_F(AutomateTests, getStatePointerMatrix3) {

        std::vector<bool> testRow1 = {true, true, false, false};
        std::vector<bool> testRow2 = {false, false, false, true};
        std::vector<bool> testRow3 = {false, false, false, true};

        // 110000010001

        std::vector<std::vector<bool>> fullTestState = {testRow1, testRow2, testRow3};

        EXPECT_EQ(3089, getCurrentStatePointer(fullTestState));

        setCurrentStatePointer(2590, fullTestState);
        EXPECT_EQ(2590, getCurrentStatePointer(fullTestState));
    }

    TEST_F(AutomateTests, getStatePointerMatrix1) {

        std::vector<bool> testRow1 = {true, true, false, false, false, false, true, true};

        // 11000011 -> 195

        std::vector<std::vector<bool>> fullTestState = {testRow1};

        EXPECT_EQ(195, getCurrentStatePointer(fullTestState));

        setCurrentStatePointer(193, fullTestState);
        EXPECT_EQ(193, getCurrentStatePointer(fullTestState));

    }

    TEST_F(AutomateTests, getStatePointerMatrix11) {

        std::vector<bool> testRow1 = {true, true, true, true, true, true, true, true};

        // 11000011 -> 195

        std::vector<std::vector<bool>> fullTestState = {testRow1};

        EXPECT_EQ(255, getCurrentStatePointer(fullTestState));

        setCurrentStatePointer(193, fullTestState);
        EXPECT_EQ(193, getCurrentStatePointer(fullTestState));

    }

    TEST_F(AutomateTests, getStatesForCycle) {

        const int dimY = 1;
        const int dimX = 8;     //3 2 1 0
        const int function = 6; //0 1 1 0

        Automate *automate = new Automate(dimX, dimY, function, FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD, 1, false);
        std::vector<std::vector<bool>> fullTestState = automate->getCurrentState();

        EXPECT_EQ(0, getCurrentStatePointer(fullTestState));
        automate->setStatePointer(14);

        for (int i = 0; i < 14; ++i) {
            automate->applyFunctionStep();
            std::cout << "step " << i << ":" << std::endl;
            std::cout << stringBinaryForIntegerWithLenght(getCurrentStatePointer(automate->getCurrentState()), 16)
                      << std::endl;
        }
    }

    TEST_F(AutomateTests, reversibilityCheck) {
        const int dimY = 1;
        const int dimX = 8;     //3 2 1 0
        const int function = 6; //0 1 1 0
        Automate *automate = new Automate(dimX, dimY, function, FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD, 1, false);

        EXPECT_EQ(-1, automate->checkIfReversible());

    }

    TEST_F(AutomateTests, reversibilitySuccessCheck) {
        const int dimY = 1;
        const int dimX = 7;
        std::string stringFunction = "0011001110010011";

        unsigned int function = std::stoi(stringFunction, 0, 2);

        Automate *automate = new Automate(dimX, dimY, function, VON_NEYMAN_NEIGHBORHOOD, 2, false);

        EXPECT_EQ(0, automate->checkIfReversible());
    }


    TEST_F(AutomateTests, testGhegalkinFormula) {
        std::vector<bool> anf = {false, true, true, false,
                                 false, false, false, false,
                                 true, false, false, false,
                                 false, false, false, false};
        std::vector<std::string> literals = {"L2", "L1", "R1", "R2"};

        Function result = getFormulaFromAnf(anf, literals);
        std::cout << result.getFormula() << "   |Type:" << result.getTypeOfFunction() << std::endl;
    }

    TEST_F(AutomateTests, testGhegalkinFormulaWithInversion) {
        std::vector<bool> anf = {true, true, true, false,
                                 false, false, false, false,
                                 true, false, false, false,
                                 false, false, false, false};
        std::vector<std::string> literals = {"L2", "L1", "R1", "R2"};

        Function result = getFormulaFromAnf(anf, literals);
        std::cout << result.getFormula() << "   |Type:" << result.getTypeOfFunction() << std::endl;
    }

    TEST_F(AutomateTests, testGhegalkinFormulaNonLinear) {
        std::vector<bool> anf = {true, true, false, true,
                                 true, true, false, true};
        std::vector<std::string> literals = {"L1", "C", "R1"};
        Function result = getFormulaFromAnf(anf, literals);
        std::cout << result.getFormula() << "   |Type:" << result.getTypeOfFunction() << std::endl;
    }

    TEST_F(AutomateTests, testGhegalkinFormulaNonLinear2) {
        std::vector<bool> anf = {false, false, false, true};
        std::vector<std::string> literals = {"L1", "R1"};
        Function result = getFormulaFromAnf(anf, literals);
        std::cout << result.getFormula() << "   |Type:" << result.getTypeOfFunction() << std::endl;
    }

    TEST_F(AutomateTests, testGhegalkinFormulaShift) {
        std::vector<bool> anf = {true, true, false, false};
        std::vector<std::string> literals = {"L2", "R2"};
        Function result = getFormulaFromAnf(anf, literals);
        std::cout << result.getFormula() << "   |Type:" << result.getTypeOfFunction() << std::endl;
    }

    TEST_F(AutomateTests, testGhegalkinFormulaShift2) {
        std::vector<bool> anf = {true, false, false, false};
        std::vector<std::string> literals = {"L2", "R2"};
        Function result = getFormulaFromAnf(anf, literals);
        std::cout << result.getFormula() << "   |Type:" << result.getTypeOfFunction() << std::endl;

        std::string shift = "Shift";
        (shift == "Shift") ? std::cout << "EQUAL" : std::cout << "NOT EQUAL";
    }

    TEST_F(AutomateTests, TestShiftedCycleDetector) {
        std::string stringFunction = "0011011000110011";
        unsigned int function = std::stoi(stringFunction, 0, 2);
        Automate *automate = new Automate(7, 1, function, VON_NEYMAN_NEIGHBORHOOD, 2, false);
        RcaCryptoAnalyzer *analyzer = new RcaCryptoAnalyzer(*automate);

        unsigned int first = std::stoi("0101110", 0, 2);
        unsigned int second = std::stoi("0010001", 0, 2);
        unsigned int third = std::stoi("1011100", 0, 2);
        std::vector<unsigned int> shiftedCycle = {first, second, third};
        for (int i = 0; i < shiftedCycle.size(); ++i) {
            std::cout<<shiftedCycle[i]<<"|";
        }
        EXPECT_EQ(true, analyzer->isCycleShifted(shiftedCycle));
    }



}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}