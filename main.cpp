#include "automate.h"
#include <omp.h>
#include <iostream>
#include <string>
#include <limits.h>

void checkRC(unsigned int dimensionX,
             unsigned int dimensionY,
             unsigned int border,
             unsigned int sizeOfBorder,
             unsigned int quantityOfFunctions,
             const std::string &directoryForDimensionAndBorder);

int main() {

    /**
    Simple reversible cellular automate
    |x|x|x|x|x|x|x|x|

    DimX 8-16
    DimY 1

    */
    std::string outputDirectory = "output";
    system(("mkdir " + outputDirectory).c_str());
    omp_set_num_threads(8);

	for (unsigned int dimensionX = 7; dimensionX <= 23; ++dimensionX) {
		std::cout<<"Current dimension:"<<dimensionX<<"x"<<1<<std::endl;

		unsigned int dimensionY = 1;
		checkRC(dimensionX, dimensionY, VON_NEYMAN_NEIGHBORHOOD,
						 1, 16, outputDirectory);
		std::cout<<"VNBorderSize1 completed"<<std::endl;
		checkRC(dimensionX, dimensionY, VON_NEYMAN_NEIGHBORHOOD,
						 1, 256, outputDirectory);
		std::cout<<"VNBorderSize2 completed"<<std::endl;

		checkRC(dimensionX, dimensionY, ZERO_VON_NEYMAN_NEIGHBORHOOD,
						 1, 16, outputDirectory);
		std::cout<<"ZVNBorderSize1 completed"<<std::endl;
		checkRC(dimensionX, dimensionY, FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD,
						 1, 256, outputDirectory);
		std::cout<<"FZVNBorderSize1 completed"<<std::endl;
//
//		checkRC(dimensionX, dimensionY, ZERO_VON_NEYMAN_NEIGHBORHOOD,
//						 2, 65536, outputDirectory);
//		std::cout<<"FVNBorderSize1 completed"<<std::endl;
//		checkRC(dimensionX, dimensionY, VON_NEYMAN_NEIGHBORHOOD,
//						 2, 65536, outputDirectory);

		std::cout<<"ZVNBorderSize2 completed"<<std::endl;

	}

    /**
    Reversible cellular automate with 2 dimensions

            X
    |x|x|.......|x|x|
    .................	Yw
    |x|x|.......|x|x|
    |x|x|.......|x|x|



    */

    // for (unsigned int dimensionX = 2; dimensionX <=6; ++dimensionX) {
    // 	for (unsigned int dimensionY = 2; dimensionY <=6; ++dimensionY) {
    // 		if (dimensionX*dimensionY >25) {
    // 			continue;
    // 		}
    // 		std::cout<<"Current dimension:"<<dimensionX<<"x"<<dimensionY<<std::endl;

    // 		checkRC(dimensionX, dimensionY, VON_NEYMAN_NEIGHBORHOOD,
    // 						 1, 65536, outputDirectory);
    // 		std::cout<<"VNBorderSize1 completed"<<std::endl;


    // 		checkRC(dimensionX, dimensionY, ZERO_VON_NEYMAN_NEIGHBORHOOD,
    // 						 1, 65536, outputDirectory);x
    // 		std::cout<<"ZVNBorderSize1 completed"<<std::endl;

    // 	}

    // }

    // 5-variables functions

//    for (unsigned int dimensionX = 7; dimensionX <= 19; ++dimensionX) {
//        std::cout << "Current dimension:" << dimensionX << "x" << 1 << std::endl;
//        unsigned int dimensionY = 1;
//
//        checkRC(dimensionX, dimensionY, FULL_VON_NEYMAN_NEIGHBORHOOD,
//                2, UINT_MAX - 1, outputDirectory);
//        std::cout << "FVNBorderSize2 completed" << std::endl;
//
//        checkRC(dimensionX, dimensionY, FULL_ZERO_VON_NEYMAN_NEIGHBORHOOD,
//                2, UINT_MAX - 1, outputDirectory);
//
//    }


    // for (unsigned int dimensionX = 2; dimensionX <=6; ++dimensionX) {
    // 	for (unsigned int dimensionY = 2; dimensionY s<=6; ++dimensionY) {
    // 		if (dimensionX*dimensionY >25) {
    // 			continue;
    // 		}

    // 		checkRC(dimensionX, dimensionY, FULL_VON_NEYMAN_BORDER,
    // 						 1, UINT_MAX, outputDirectory);
    // 		std::cout<<"FVNBorderSize1 completed"<<std::endl;
    // 		checkRC(dimensionX, dimensionY, FULL_ZERO_VON_NEYMAN_BORDER,
    // 						 1, UINT_MAX, outputDirectory);

    // 		std::cout<<"FZVNBorderSize1 completed"<<std::endl<<std::endl;
    // 	}
    // }



    return 0;

}

void checkRC(unsigned int dimensionX,
             unsigned int dimensionY,
             unsigned int neighborhood,
             unsigned int sizeOfNeighborhood,
             unsigned int quantityOfFunctions,
             const std::string &directoryForDimensionAndBorder) {
#pragma omp parallel for
    for (unsigned int function = 0; function < quantityOfFunctions; ++function) {

        if (quantityOfFunctions > 65536) {
            // function with 5 variables
            if (std::bitset<32>(function).count() != 16) {
                continue;
            }
        }

        if (function & (1 << 12) != 0) {
            std::cout << "2^12 functions was considered" << std::endl;
        }

        auto *currentAutomate = new Automate(dimensionX,
                                             dimensionY,
                                             function,
                                             neighborhood,
                                             sizeOfNeighborhood,
                                             true);

        int result = currentAutomate->checkIfReversible();
        if (result == 0) {
            currentAutomate->writeGraphToFile(directoryForDimensionAndBorder);
        }

        delete currentAutomate;
    }
}
